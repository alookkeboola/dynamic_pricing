####################################################################################

# Created on 2016-04-13

# @copyright:   aLook Analytics
# @author:      Martin
# @client:      Keboola

# @use:         Calculate the demand function based on historical data pf products
#               The calculation is done by normalizing groups, because for some products
#               the amount of data was insufficient to create robust demand functions
# @dependency:  none
# @input:       input files - need to specify names in script
# @output:      output files - - need to specify names in script

# @version:     1.0

####################################################################################

import os, re
import numpy as np
from scipy import optimize as optimize

# =============================================================================
# DEFINE FUNCTIONS
# =============================================================================

#inline functions
convertfunc = lambda x:  (re.sub('"','',x.decode('utf8'))[:10])
convertfunc2 = lambda x: (re.sub('"',"",(x.decode('utf8'))))

def convertfunc3(x):
    '''
    Function to remove empty strings and insert a 0 instead for the category id
    '''
    try:
        return int((re.sub('"',"",(x.decode('utf8')))))
    except:
        return 0

def normalize(id_product,price):
    '''
    Normalize the values for a given product
    :param id_product: product identification (to find values for transformation
    :param price: input price
    :return: output price after transformation if possible, else return -1
    '''
    for tuple in result_list:
        #find the record in the result list
        if tuple[0] == id_product:
            mean = tuple[2]
            std  = tuple[3]
            break
    if mean != 0 and std != 0:
        return (price - mean)/std
    else:
        return -1
def denormalize(id_product,price):
    '''
    Denormalize the values for a given product
    :param id_product: product identification (to find values for transformation
    :param price: input price
    :return: output price after transformation
    '''
    for tuple in result_list:
        if tuple[0] == id_product:
            mean = tuple[2]
            std  = tuple[3]
            break

    return ((price *std) + mean)

def demand_function(alpha, beta, pieces):
    '''
    demand function to estimate price from quantity
    :param parameters: input parameters of the linear function to estimate price
    :param x: the number of items to be sold
    :return:
    '''
    a = alpha
    b = beta
    if pieces > pieces_per_day:
        return a*pieces+b#+a*((x-pieces_per_day)**2) '''*((pieces-pieces_per_day))'''
    else:
        return a*pieces+b

def demand_func_exp(pieces):
    return alpha_exp*np.exp(-beta_exp*pieces)+gama_exp

def exp_func(x, a, c, d):
    '''
    function serves for optimization, because if I use the other version, then the
    optimizer has problems determining the number of arguments to optimize (not explicitly stated)
    :param x:
    :param a:
    :param c:
    :param d:
    :return:
    '''
    return a*np.exp(-c*x)+d

def profit_function(pieces):
    '''

    :param inventory: total number of items
    :param minimal_price: minimal admissible price
    :return: returns estimated price
    '''
    price = demand_function(alpha, beta,pieces)
    #if price < normalize(element,minimal_price):
    #    price = normalize(element,0)
    return price

def maximization(input):
    '''
    :param input:
    [0]=time_steps
    [1]=inventory
    [2]=minimal_price
    :return:
    '''
    sum   = 0
    if demand_fun_type == 'linear':
        for i in range (1,time_steps+1):
            sum = sum + denormalize(element,profit_function(pieces     = input[i-1])) *input[i-1] * (risk_aversion**i)

        sum = sum + input[time_steps] *salvage_price
    elif demand_fun_type == 'exponential':
        for i in range (1,time_steps+1):
            sum = sum + denormalize(element,demand_func_exp(pieces     = input[i-1])) *input[i-1] * (risk_aversion**i)

        sum = sum + input[time_steps] *salvage_price

    return -sum

def set_data_for_maximization(id_product):
    global inventory
    global time_steps
    global salvage_price
    global pieces_per_day
    global alpha
    global beta
    global alpha_exp
    global beta_exp
    global gama_exp

    for tuple in result_list:
        if tuple[0] == id_product:
            pieces_per_day    = tuple[1]/tuple[4]
            id_category       = tuple[5]
            break

    for category in category_parameters:
        if category[0] == id_category:
            alpha          = category[1]
            beta           = category[2]
            alpha_exp      = category[3]
            beta_exp       = category[4]
            gama_exp       = category[5]
            break

    inventory      = 10
    time_steps     = 10
    salvage_price  = 10

    #define the bounds
    bnds           = [(0,inventory) for i in range (0,time_steps+1) ]
    starting_point = [(1) for i in range (0,time_steps+1) ]
    #constraint to ensure that all products have either been sold out of or kept in the inventory
    cons           = ({'type': 'eq', 'fun': lambda x:  inventory - sum(x)})



    return  (optimize.minimize(fun         = maximization,
                              x0          = starting_point,
                              bounds      = bnds,
                              constraints = cons,
                              method      ='SLSQP',
                              #callback=callbackF
                              ))

def callbackF(Xi):
    '''
    Function to give the results of the optimization evaluations
    :param Xi: individual amounts
    :return:
    '''
    global Nfeval
    prc = denormalize(element,profit_function(pieces     = Xi[0]))
    profit = prc * Xi[0]+ 20 * Xi[1]
    print ('{0:4d}   {1: 3.6f}   {2: 3.6f} {3: 3.6f} {4: 3.6f}'.format(Nfeval, Xi[0], Xi[1], prc, profit))
    Nfeval += 1

# =============================================================================
# Main
# =============================================================================

import csv

# CSV format settings

csvlt = '\n'
csvdel = ','
csvquo = '"'

# initialize application
from keboola import docker
cfg = docker.Config('/data/')

#load input parameters
inventory       = cfg.get_parameters('inventory')
time_steps      = cfg.get_parameters('time_steps')
salvage_price   = cfg.get_parameters('salvage_price')
demand_fun_type = cfg.get_parameters('function_type')
risk_aversion   = cfg.get_parameters('risk_aversion')
in_product      = cfg.get_parameters('product')
in_category      = cfg.get_parameters('category')



if __name__ == "__main__":

    #LOAD DATA from csv file
    data = np.genfromtxt('/data/in/tables/pricing_dataset.csv',
                         skip_header=1,
                         delimiter=',',
                         dtype=[ ('id_product', np.str_,32),
                                 ('id_client', np.str_,32),
                                 ('id_category', int),
                                 ('quantity', float),
                                 ('price', float),
                                 ('date', 'M8[D]')],
                         converters={0: convertfunc2,
                                     1: convertfunc2,
                                     2: convertfunc3,
                                     3: convertfunc2,
                                     4: convertfunc2,
                                     5: convertfunc},
                         missing_values={2: 0})


    #get the unique values for each category(to be used for cycle)
    products    = (np.unique(data[:]['id_product']))
    clients     = (np.unique(data[:]['id_client']))
    categories  = list((np.unique(data[:]['id_category'])))

    #define the result of going through the matrix
    result_list = []

    for product in products:
        #if product == '243630eead7a87c8245ae0f3f721bd63':
        #restrict data to selected product
        x = (data[data[:]['id_product'] == product])

        #calculate the number of days for which we hve data
        days = (np.amax(x[:]['date'])- np.amin(x[:]['date'])).astype(int)
        if days == 0:
            days = 1

        #calculate aggregated values for the product
        product_summary = (product,
                            np.sum(x[:]['quantity']),
                            np.average(x[:]['price']),
                            np.std(x[:]['price']),
                            days,
                            x[0]['id_category'] )

        #if at least some pieces were sold add the product to the result list
        if product_summary[1] != 0:

            result_list.append(product_summary)

    #define the variable to keep the parameters of the demand function
    category_parameters=[]

    #go through all existing categories
    for category in categories:
        price_value = dict()

        #Get all records from the given category
        x = (data[data[:]['id_category'] == category])
        for element in x:
            #normalize the values
            norm = normalize(element[0],element[4])
            #print (norm)
            # for -1 he value could not be calculated se we do not proceed
            if norm != -1:
                #for the given price sum up the quantity from each record
                if norm in price_value.keys():
                    #if the price already exists add the quantity
                    price_value[norm] = price_value[norm] + element[3]
                else:
                    #else create a new record in the dictonary for the price value pair
                    price_value[norm] = int(element[3])

        #fit a linear function + exponential function based on the normalized price values pairs
        try:
            parameters           = (np.polyfit(list(price_value.values()),list(price_value.keys()), deg=1))
            parameters_exp, pcov = optimize.curve_fit(exp_func, list(price_value.values()), list(price_value.keys()))

        except:
            #if we are not able to calculate the selected category, then remove it
            categories.remove(category)


        #add the result of the fitting to the list
        category_parameters.append((category,
                                    parameters[0],
                                    parameters[1],
                                    parameters_exp[0],
                                    parameters_exp[1],
                                    parameters_exp[2]))

    #-----------------------------------------------------------------------------------#

    #output list declared
    output=[]
    #output header list declared
    output_header = []
    #declare output for the prices of the optimization at each period + the identification of the product and
    #the remaining inventory
    for i in range (0, time_steps+2):
        if i == 0:
            output_header.append(('sku'))
        elif i > 0 and i < (time_steps+1):
            output_header.append(('period_'+str(i)))
        else:
            output_header.append('inventory_left')

    output.append(output_header)

    #the data are prepared now the optimization starts for each element
    for element in products:

        #output row for individual products
        product_output= list()

        #write the product name to the list
        product_output.append(element)

        #function to get the required parameters for the given product + optimize vakues
        res = set_data_for_maximization(id_product = element )

        for i in range(0,len(res['x'])):
            if i < len(res['x'])-1:
                product_output.append(denormalize(element, res['x'][i]))
            else:
                product_output.append(round(res['x'][i],2))
        output.append(product_output)

    f_out = open('/data/out/tables/alook_result.csv', mode='w')
    for element in output:
        f_out.writelines(','.join(str(v) for v in element)+'\n')
    f_out.close()


